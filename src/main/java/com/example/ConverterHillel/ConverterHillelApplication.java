package com.example.ConverterHillel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConverterHillelApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConverterHillelApplication.class, args);
	}

}
